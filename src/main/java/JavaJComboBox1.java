
import javax.swing.JComboBox;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJComboBox1 {

    public static void main(String[] args) {

        JFrame frame = new JFrame("ComboBox Example");
        frame.setSize(400, 500);
        frame.setLayout(null);
        frame.setVisible(true);

        String country[] = {"India", "Aus", "U.S.A", "England", "Newzealand"};

        JComboBox cbb = new JComboBox(country);
        cbb.setBounds(50, 50, 90, 20);
        frame.add(cbb);

    }

}
