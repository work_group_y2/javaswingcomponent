
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
class MyJComponent extends JComponent {

    public void paint(Graphics g) {
        g.setColor(Color.green);
        g.fillRect(300, 300, 100, 100);
    }
}

public class JavaJComponent {

    public static void main(String[] args) {

        MyJComponent com = new MyJComponent();
        // create a basic JFrame  
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // add the JComponent to main frame  
        frame.add(com);
        frame.setVisible(true);
        
        
        

    }

}
