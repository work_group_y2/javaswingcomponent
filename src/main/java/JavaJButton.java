
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJButton {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example"); //สร้างหน้าต่างโปรแกรม 
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

        final JTextField txt = new JTextField(); //สร้างกล่องพิมพ์ข้อความ
        txt.setBounds(50, 50, 150, 20);
        frame.add(txt);

        JButton btnclick = new JButton("Click Here"); //สร้างปุ่มคลิก
        btnclick.setBounds(50, 100, 95, 30);
        btnclick.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                txt.setText("Welcome to Javapoint."); //กด ปุ่มcilck ก็จะแสดงข้อความนี้

            }

        });

        frame.add(btnclick);

    }
}
