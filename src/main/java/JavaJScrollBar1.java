
import javax.swing.JFrame;
import javax.swing.JScrollBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJScrollBar1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Scrollbar Example");
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        
        JScrollBar sb = new JScrollBar();
        sb.setBounds(100, 100, 50, 100);
        frame.add(sb);
        
    }
}
