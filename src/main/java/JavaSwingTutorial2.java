
import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaSwingTutorial2 {

    JFrame frame;

    JavaSwingTutorial2() {
        frame = new JFrame();//creating instance of JFrame  

        JButton btn = new JButton("click");//creating instance of JButton  
        btn.setBounds(130, 100, 100, 40);

        frame.add(btn);//adding button in JFrame  

        frame.setSize(400, 500);//400 width and 500 height  
        frame.setLayout(null);//using no layout managers  
        frame.setVisible(true);//making the frame visible  
    }

    public static void main(String[] args) {
        new JavaSwingTutorial2();
    }
}
