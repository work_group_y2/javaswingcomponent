
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class MyCanvas extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("p3.gif");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        MyCanvas m = new MyCanvas();
        JFrame frame = new JFrame();
        frame.add(m);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

}
