
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJSlider1 extends JFrame {

    public JavaJSlider1() {

        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);

        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);

    }

    public static void main(String[] args) {
        JavaJSlider1 frame = new JavaJSlider1();
        frame.pack();
        frame.setVisible(true);

    }
}
