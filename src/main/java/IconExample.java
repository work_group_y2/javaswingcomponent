
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class IconExample {

    public static void main(String[] args) {
        Frame frame = new Frame();
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);

        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\icon.png");
        frame.setIconImage(icon);
    }

}
