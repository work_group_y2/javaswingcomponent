
import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaSwingTutorial3 extends JFrame {

    JFrame frame;

    JavaSwingTutorial3() {
        JButton btn = new JButton("click");//create button  
        btn.setBounds(130, 100, 100, 40);

        add(btn);//adding button on frame  
        setSize(400, 500);
        setLayout(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        new JavaSwingTutorial3();
    }
}
