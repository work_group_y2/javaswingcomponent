
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJProgressBar1 extends JFrame {

    JProgressBar pb;

    int i = 0, num = 0;

    JavaJProgressBar1() {
        pb = new JProgressBar(0, 2000);
        pb.setBounds(40, 40, 160, 30);
        pb.setValue(0);
        pb.setStringPainted(true);
        add(pb);
        setSize(250, 150);
        setLayout(null);

    }

    public void iterate() {
        while (i <= 2000) {
            pb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }

        }
    }

    public static void main(String[] args) {
        JavaJProgressBar1 jpb = new JavaJProgressBar1();
        jpb.setVisible(true);
        jpb.iterate();

    }
}
