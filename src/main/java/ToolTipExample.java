
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class ToolTipExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        //Adding components to frame  
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        //Creating PasswordField and label  
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        value.setToolTipText("Enter your Password");

        JLabel l1 = new JLabel("Password:");
        l1.setBounds(20, 100, 80, 30);

        frame.add(value);
        frame.add(l1);
    }
}
