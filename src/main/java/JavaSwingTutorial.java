
import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaSwingTutorial {

    public static void main(String[] args) {
        JFrame frame = new JFrame(); //สร้าง instance ของ JFrame
        frame.setSize(400, 500); // กว้าง 400 สูง 500
        frame.setLayout(null); // โดยไม่ต้องใช้ตัวจัดการเลย์เอาต์
        frame.setVisible(true); // ทำให้มองเห็นกรอบได้
        
        JButton lblclick = new JButton("click"); //สร้าง instance ของ JFrame
        lblclick.setBounds(130, 100, 100, 40); // แกน x, แกน y, กว้าง, สูง
        frame.add(lblclick); //เพิ่มปุ่มใน JFrame
    }
}
