
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class JavaJPopupMenu1 {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("PopupMenu Example");
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        final JPopupMenu popupmenu = new JPopupMenu("Edit");

        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");

        popupmenu.add(cut);
        popupmenu.add(copy);
        popupmenu.add(paste);

        frame.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                popupmenu.show(frame, e.getX(), e.getY());
            }
        });

        frame.add(popupmenu);
    }
}
